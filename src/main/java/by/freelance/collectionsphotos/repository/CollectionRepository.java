package by.freelance.collectionsphotos.repository;

import by.freelance.collectionsphotos.entity.CollectionEntity;
import by.freelance.collectionsphotos.entity.UserEntity;

import java.util.List;

public interface CollectionRepository {

    CollectionEntity findById(int id);

    List<CollectionEntity> findAll();

    CollectionEntity create(CollectionEntity entity);

    CollectionEntity update(CollectionEntity entity);

    void deleteById(int id);

    void deleteAll();

}
