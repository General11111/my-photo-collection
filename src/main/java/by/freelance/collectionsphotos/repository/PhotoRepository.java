package by.freelance.collectionsphotos.repository;

import by.freelance.collectionsphotos.entity.CollectionEntity;
import by.freelance.collectionsphotos.entity.PhotoEntity;

import java.util.List;

public interface PhotoRepository {

    PhotoEntity findById(int id);

    List<PhotoEntity> findAll();

    PhotoEntity create(PhotoEntity entity);

    PhotoEntity update(PhotoEntity entity);

    void deleteById(int id);

    void deleteAll();

}
