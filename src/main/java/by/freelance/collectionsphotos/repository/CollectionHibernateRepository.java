package by.freelance.collectionsphotos.repository;

import by.freelance.collectionsphotos.entity.CollectionEntity;
import by.freelance.collectionsphotos.entity.UserEntity;
import by.freelance.collectionsphotos.utils.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class CollectionHibernateRepository implements CollectionRepository {
    @Override
    public CollectionEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        CollectionEntity foundCollection = em.find(CollectionEntity.class, id);

        em.getTransaction().commit();
        em.close();
        return foundCollection;
    }

    @Override
    public List<CollectionEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List<CollectionEntity> allCollections = em
                .createNativeQuery("SELECT * FROM collections", CollectionEntity.class)
                .getResultList();

        em.getTransaction().commit();
        em.close();
        return allCollections;
    }

    @Override
    public CollectionEntity create(CollectionEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(entity);

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public CollectionEntity update(CollectionEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(entity);

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        CollectionEntity entityToDelete = em.find(CollectionEntity.class, id);

        em.remove(entityToDelete);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collections")
                .executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
