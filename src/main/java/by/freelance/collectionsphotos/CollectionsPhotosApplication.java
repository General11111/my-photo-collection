package by.freelance.collectionsphotos;


import by.freelance.collectionsphotos.entity.CollectionEntity;
import by.freelance.collectionsphotos.entity.UserEntity;
import by.freelance.collectionsphotos.repository.CollectionHibernateRepository;
import by.freelance.collectionsphotos.repository.CollectionRepository;
import by.freelance.collectionsphotos.repository.UserHibernateRepository;
import by.freelance.collectionsphotos.repository.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;

import java.util.List;

@SpringBootApplication
public class CollectionsPhotosApplication {

	public static void main(String[] args) {

		SpringApplication.run(CollectionsPhotosApplication.class, args);

		System.out.println("создаю репозиторий");
		UserRepository userRepository = new UserHibernateRepository();

//		System.out.println("создаю энтити");
//		UserEntity userEntity = new UserEntity();
//		userEntity.setLogin("admin2");
//		userEntity.setPassword("1234qweer");
//		userEntity.setEmail("bob2@gmail.com");
//		userEntity.setName("bob2");
//
//		System.out.println("олправляю в репозиторий");
//
//		repository.create(userEntity);
//		System.out.println("готово");
//		System.out.println(userEntity);

//		repository.deleteAll();
//		repository.deleteById(3);

		List<UserEntity> ue = userRepository.findAll();
		System.out.println(ue);

		UserEntity user = userRepository.findById(1);
		System.out.println("Name: " + user.getName());
		System.out.println("Col collections: " + user.getCollections().size());




//		UserEntity existingUser = repository.findById(1);
//		CollectionEntity collection = new CollectionEntity();
//		collection.setName("Summer-2021");
//		collection.setDescription("Cool time!");
//		collection.setUser(existingUser);
//		CollectionRepository collectionRepository = new CollectionHibernateRepository();
//		collectionRepository.create(collection);


	}


}
